import {useState, useEffect} from 'react';
import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import {useParams, useNavigate} from 'react-router-dom';
import {useContext} from 'react';
import Swal from 'sweetalert2';
import UserContext from '../UserContext'
import {Navigate} from 'react-router-dom';

export default function ProductView(){
	const [productName, setProductName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0)

	const history = useNavigate()

	const {productId} = useParams();

	const {user, setUser} = useContext(UserContext);

	useEffect(()=>{
		fetch(`${process.env.REACT_APP_URI}/products/${productId}`)
		.then(response => response.json())
		.then(data =>{
			console.log(data);
			setProductName(data.productName);
			setDescription(data.description);
			setPrice(data.price);
		})
	}, [productId])

	const purchase = (productid) =>{
		fetch(`${process.env.REACT_APP_URI}/users/checkout/${productId}`, {
			method: "POST",
			headers: {
				"Content-Type" : "application/json",
				Authorization : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(data => {
			console.log(data)
			if(data === true){
				Swal.fire({
					title: "Checked-out Successfully!",
					icon: "success",
					text: "Thank you for buying"
				})
				history("/products")
			}
			else{
				Swal.fire({
					title: "Admins are not allowed to checkout!",
					icon: "error",
					text: "Login using a regular user account."
				})
				history("/");
			}
		})
	}

	return(
		(user.id == null) 
		?	<Navigate to="/login" />
		:
		<Container>
			<Row>
				<Col lg={{span: 6, offset: 3}}>
					<Card>
					    <Card.Body className="">
							<Card.Title>{productName}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>
							{/*<Card.Subtitle>Sold: 243</Card.Subtitle>*/}
							<Card.Text>Rating: 4/5</Card.Text>
							<Button variant="dark" onClick={()=>purchase(productId)} >Check Out</Button>
						</Card.Body>	
					</Card>
				</Col>
			</Row>
		</Container>
		)
}