import {Container, Row, Col, Button, Form} from 'react-bootstrap';
import {useNavigate, useParams} from 'react-router-dom';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';


export default function UpdateProduct(prop){

	const [productName, setProductName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0)
	const [stocks, setStocks] = useState(0);

	const {productId} = useParams();

	const navigateToDashboard = useNavigate();

	useEffect(()=>{
		fetch(`${process.env.REACT_APP_URI}/products/${productId}`)
		.then(response => response.json())
		.then(data =>{
			console.log(data);
			setProductName(data.productName);
			setDescription(data.description);
			setPrice(data.price);
			setStocks(data.stocks);
		})
	}, [productId])

	const editProduct = (productid) =>{
		fetch(`${process.env.REACT_APP_URI}/products/update/${productId}`, {
			method: "PUT",
			headers: {
				"Content-Type" : "application/json",
				Authorization : `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productName: productName,
				description: description,
				price: price,
				stocks: stocks,
			})
		})
		.then(response => response.json())
		.then(data => {
			console.log(data)

			Swal.fire({
						title: "Changes has been saved!",
						icon: "success",
						text: ""
					})
			navigateToDashboard("/admin");
		})
	}

	return(
		<Container>
			<Row>
				<Col className = "colForm">
					<div className=''>
					<Form  className="addProductForm p-3 col-md-4 col-8 offset-4 text-white">
					  <Form.Group className="mb-3" controlId="firstName">
					    <Form.Label>Product Name</Form.Label>
					    <Form.Control 
					    	type="name" 
					    	value = {productName}
					    	onChange  = {event => setProductName(event.target.value)}
					    	required/>
					  </Form.Group>

					  <Form.Group className="mb-3" controlId="lname">
					    <Form.Label>Description</Form.Label>
					    <Form.Control 
					    	type="name" 
					    	value = {description}
					    	onChange  = {event => setDescription(event.target.value)}
					    	required/>
					  </Form.Group>

					  <Form.Group className="mb-3" controlId="email">
					    <Form.Label>Price</Form.Label>
					    <Form.Control 
					    	type="number" 
					    	value = {price}
					    	onChange  = {event => setPrice(event.target.value)}
					    	required/>
					  </Form.Group>

					  <Form.Group className="mb-3" controlId="mobileNumber">
					    <Form.Label>Stocks</Form.Label>
					    <Form.Control 
					    	type="number" 
					    	value = {stocks}
					    	onChange  = {event => setStocks(event.target.value)}
					    	required/>
					  </Form.Group>

					  <Button 
						  variant="primary" 
						  onClick={()=>editProduct(productId)}
						  
						  >Save Changes
					  </Button>
					</Form>
					</div>
				</Col>
			</Row>
		</Container>
		)
}