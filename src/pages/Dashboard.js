import {Fragment, useEffect, useState} from 'react';
import AdminDashboard from '../components/AdminDashboard';
import UpdateProduct from './UpdateProduct';

export default function Products(){

	const [products, setProducts] = useState([]);

	useEffect(()=>{
		fetch(`${process.env.REACT_APP_URI}/products/allProducts`,{
			headers: {
				'Content-Type' : 'application/json',
				Authorization : `Bearer ${localStorage.getItem('token')}`
			}}
			)
			.then(response => response.json())
			.then(data =>{

				setProducts(data.map(product => {
					return (
							<AdminDashboard key={product._id} prop={product} />
					)
				}))
			})
	}, [])

	return(
		<Fragment>
			{products}
		</Fragment>
		)
}
