import {Container, Row, Col, Button, Form} from 'react-bootstrap';

import {useNavigate} from 'react-router-dom';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';


export default function CreateProduct(){

	const [productName, setProductName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");
	const [stocks, setStocks] = useState("");

	const [isActive, setIsActive] = useState(false)

	const {user, setUser} = useContext(UserContext);

	const navigateToDashboard = useNavigate();

	useEffect(()=>{
		if (productName !== "" && price !== "" && description !== "" && stocks !== ""){
				setIsActive(true);
		}
		else{
			setIsActive(false)
		}

	}, [productName, description, price, stocks])

	console.log(user.isAdmin)

	function addProduct(event){
		event.preventDefault()

		fetch(`${process.env.REACT_APP_URI}/products/addProduct`, {
			method: "POST",
			headers: {
				'Content-Type' : 'application/json',
				Authorization : `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productName: productName,
				description: description,
				price: price,
				stocks: stocks,
			})
		}).then(response => response.json())
		.then(data =>{

			console.log(data);

			if(data === true){
				Swal.fire({
					title: "New Product Created!",
					icon: "success",
					text: ""
				})
				navigateToDashboard("/admin");
			}
			else{
				Swal.fire({
					title: "Something went wrong!",
					icon: "error",
					text: "Please try again"
				})
				navigateToDashboard("/admin");
			}
		})
	}

	return(
		<Container>
			<Row>
				<Col className = "colForm">
					<div className=''>
					<Form onSubmit={addProduct} className="align-center offset-4 addProductForm p-3 col-md-4 col-8 text-white">
					  <Form.Group className="mb-3" controlId="firstName">
					    <Form.Label>Product Name</Form.Label>
					    <Form.Control 
					    	type="name" 
					    	value = {productName}
					    	onChange  = {event => setProductName(event.target.value)}
					    	required/>
					  </Form.Group>

					  <Form.Group className="mb-3" controlId="lname">
					    <Form.Label>Description</Form.Label>
					    <Form.Control 
					    	type="name" 
					    	value = {description}
					    	onChange  = {event => setDescription(event.target.value)}
					    	required/>
					  </Form.Group>

					  <Form.Group className="mb-3" controlId="email">
					    <Form.Label>Price</Form.Label>
					    <Form.Control 
					    	type="number" 
					    	value = {price}
					    	onChange  = {event => setPrice(event.target.value)}
					    	required/>
					  </Form.Group>

					  <Form.Group className="mb-3" controlId="mobileNumber">
					    <Form.Label>Stocks</Form.Label>
					    <Form.Control 
					    	type="number" 
					    	value = {stocks}
					    	onChange  = {event => setStocks(event.target.value)}
					    	required/>
					  </Form.Group>

					  <Button 
						  variant="primary" 
						  type="submit"
						  disabled={!isActive}
						  >Create New Product
					  </Button>
					</Form>
					</div>
				</Col>
			</Row>
		</Container>
		)
}