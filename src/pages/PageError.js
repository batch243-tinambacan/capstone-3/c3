import {Fragment} from 'react'
import {Link} from 'react-router-dom'


export default function PageError(){
	
	
	return(
		<Fragment>
			<div className='text-center'>
				<h1>Page Not Found</h1>
				<h5>Go Back To
					<Link to="/">Homepage</Link>
				</h5>
			</div>
		</Fragment>
		)

}