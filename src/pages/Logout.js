import {Navigate} from 'react-router-dom'
import UserContext from '../UserContext'
import {useContext, useEffect} from 'react';
import Swal from 'sweetalert2';

export default function Logout(){

	const {setUser, unSetUser} = useContext(UserContext);

	unSetUser();

	useEffect(()=>{
		setUser({id: null, isAdmin:false});
	}, [])
	
	Swal.fire({
		title: "You have logged out.",
		icon: "info",
		text: "Come back anytime!"
	})

	return(
		<Navigate to="/login" />
		)
}

