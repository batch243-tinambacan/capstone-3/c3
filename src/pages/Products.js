import {Fragment, useEffect, useState} from 'react';
import ProductCard from '../components/ProductCard';

export default function Products(){

	const [products, setProducts] = useState([]);

	useEffect(()=>{
		fetch(`${process.env.REACT_APP_URI}/products/allActiveProducts`)
			.then(response => response.json())
			.then(data =>{
				console.log(data);

				setProducts(data.map(product => {
					return (
						<ProductCard key={product._id} prop={product} />
					)
						}))

		})	
	}, [])


	return(
		<Fragment>
			{products}
		</Fragment>
		)
}
