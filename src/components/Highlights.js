import imgp1 from '../img/imgp1.jpg'
import imgp2 from '../img/imgp2.jpg'
import imgp3 from '../img/imgp3.jpg'

export default function Highlights(){
	return(
		<div className='backgroundHighlight p-5'>

			<h1 className='newArrival'>New Arrivals</h1>
			<h4 className='newArrival2'>Be prepared to be blown with spectacular products</h4>

			<div class="d-flex flex-md-row flex-column justify-content-md-around mt-2 align-items-center pb-3">
			{/*first card*/}
				<div class="card col-md-3 col-10 mt-md-0 mt-2">
				  <img src={imgp1} className="imgp card-img-top p-2" alt="BS Image" />
				  <div class="card-body">
				    <h5 class="card-title">Fryda</h5>
				    <p class="card-text">Plain Sling Bag</p>
				    <a href="/products" className="btn btn-secondary shadow-none">View Product</a>
				  </div>
				</div>
				{/*End of First Card*/}

				{/*Second Card*/}
				<div class="card col-md-3 col-10 mt-md-0 mt-2">
				  <img src={imgp2} className="imgp card-img-top p-2" alt="HTML Image" />
				  <div class="card-body">
				    <h5 class="card-title">Amara</h5>
				    <p class="card-text">Printed Sling Bag</p>
				    <a href="/products" className="btn btn-secondary shadow-none">View Product</a>
				  </div>
				</div>
				{/*End od Second Card*/}

				{/*Third Card*/}

				<div class="card col-md-3 col-10 mt-md-0 mt-2">
				  <img src={imgp3} className="imgp card-img-top p-2" alt="HTML Image" />
				  <div class="card-body">
				    <h5 class="card-title">Fillana</h5>
				    <p class="card-text">Convertible bag</p>
				    <a href="/products" className="btn btn-secondary shadow-none">View Product</a>
				  </div>
				</div>
				{/*End of the Third Card*/}
			</div>	
		</div>
		)
}