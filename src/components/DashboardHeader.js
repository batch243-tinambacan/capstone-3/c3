import {Container, Row, Col, Card, Button, Table} from 'react-bootstrap'
import {Link} from 'react-router-dom';

export default function AddProduct() {

	return(
		<Container>
		<Row>
		<Col>
			<h3 className='text-center'>Admin Dashboard</h3>
			<div className='my-3 text-center'>
				<Button as={Link} to='/CreateProduct' className='adminButton mx-1 btn-secondary shadow-none'>Add New Product</Button>
				{/*<Button as={Link} to='/UpdateProduct' className='adminButton mx-1'>Update Product</Button>*/}
				{/*<Button className='adminButton mx-1'>Show User Orders</Button>*/}
			</div>
			<Table className="table"striped bordered hover >
			   	<thead>
			        <tr className="tableHeader">
			          <th className="tableRow">Product Name</th>
			          <th className="tableRow">Description</th>
			          <th className="tableRow2">Price</th>
			          <th className="tableRow2">Stocks</th>
			          <th className="tableRow2">Availability</th>
			          <th className="tableRow2">Actions</th>
			          <th className="tableRow2">Update</th>
			        </tr>
			    </thead>
			</Table>
		</Col>
		</Row>
		</Container>
		)
}