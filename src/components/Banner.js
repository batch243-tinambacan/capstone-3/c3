import {Row, Col} from 'react-bootstrap'
import {Link} from 'react-router-dom';


export default function Banner(){

	return(
		<Row className="femininity text-center">
			<Col>
				<h1>Embrace your femininity</h1>
			</Col>
		</Row>
		)
}