import Carousel from 'react-bootstrap/Carousel';
import imgs2 from '../img/imgs2.jpg'
import imgs3 from '../img/imgs3.jpg'
import imgs4 from '../img/imgs4.jpg'

function DarkVariantExample() {
  return (
    <div className="slider-container col-md-12 img-fluid">
    <Carousel variant="dark">
      <Carousel.Item>
        <img
          className="d-block w-100 "
          src={imgs2}
          alt="First slide"
        />
        <Carousel.Caption>
        <a href='/products' className='text-decoration-none text-dark'>
          <h1 className='card card-bg-white'>View The Collections</h1>
        </a>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src={imgs3}
          alt="Second slide"
        />
        <Carousel.Caption>
         <a href='/products' className='text-decoration-none text-dark'>
          <h1 className='card card-bg-white'>Shop Now!</h1>
        </a>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src={imgs4}
          alt="Third slide"
        />
        <Carousel.Caption>
         <a href='/products' className='text-decoration-none text-dark'>
          <h1 className='card card-bg-white'>Everyday Luxury</h1>
        </a>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
    </div>
  );
}



export default DarkVariantExample;