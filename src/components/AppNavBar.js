import UserContext from '../UserContext'

import {Container, Nav, Navbar} from 'react-bootstrap';
import {NavLink} from 'react-router-dom';
import {Fragment, useContext} from 'react'


export default function AppNavBar(){ 

	const {user} = useContext(UserContext);

	return(
		<div className="naviBar vw-100 mb-3" expand="lg" fixed='top'>
			<h1 className="shopName text-center">Bag◇Envy</h1>
			{/*<h1 className="shopName text-center">Shop◇mera</h1>*/}
				<Nav className="navLinks justify-content-center" activeKey="/home">
				          <Nav.Link as={NavLink} to="/" className="navLink">HOME</Nav.Link>
				          <Nav.Link as={NavLink} to="/products" className="navLink">SHOP</Nav.Link>
				        	{
				          	(user.id !== null) 
				          	?	(user.isAdmin === true)
				          		?	<>
				          			<Nav.Link as={NavLink} to="/admin" className="navLink">ADMIN DASHBOARD</Nav.Link>
				          			<Nav.Link as={NavLink} to="/logout" className="navLink">Logout</Nav.Link>
				          			</>
				          		:
				          	<>
				          	<Nav.Link as={NavLink} to="/logout" className="navLink">LOGOUT</Nav.Link>
				          	</>
				          	:
				          	<Fragment>
				          	<Nav.Link as={NavLink} to="/register" className="navLink">REGISTER</Nav.Link>
							<Nav.Link as={NavLink} to="/login" className="navLink">LOGIN</Nav.Link>
							</Fragment>
				        	}
				</Nav>
		</div>
	)
}