import {Container, Row, Col, Card, Button, Table} from 'react-bootstrap'
import {useState, useEffect} from 'react';
import {useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';

export default function AdminDashboard(prop){ 

	const {_id, productName, description, price, stocks, action} = prop.prop;
	console.log(_id)

	const [isActive, setIsActive] = useState(prop.prop.isActive);

	const archiveUnarchive = (productid) =>{
		fetch(`${process.env.REACT_APP_URI}/products/${_id}/archive`, {
			method: "PATCH",
			headers: {
				"Content-Type" : "application/json",
				Authorization : `Bearer ${localStorage.getItem('token')}`
			}
		})	
		.then(response => response.json())
		.then(data => {
			console.log(data)
			if(data === true){
				Swal.fire({
					title: "Success!",
					icon: "success",
					text: "Availability changed"
				})
			}
			else{
				Swal.fire({
					title: "Something went wrong!",
					icon: "error",
					text: ""
				})
			}
		})
	}

	return(
		<Container>
			<Row>
				<Col>
					<div >
						<Table className="table"striped bordered hover >
						      <tbody>
						        <tr>
						          <td className="tableRow">{productName}</td>
						          <td className="tableRow">{description}</td>
						          <td className="tableRow2">{price}</td>
						          <td className="tableRow2">{stocks}</td>
						          {
						          	(isActive) 
						          	?	<td className="tableRow2">Available</td>
						          	: 	<td className="tableRow2">Unavailable</td>
						          }
						          <td className="tableRow2">
						          {
						          	(isActive) 
						          	?	<button onClick={()=>{setIsActive(!isActive); return archiveUnarchive(_id)}} className="archiveBorder">Deactivate</button>
						          	: 	<button onClick={()=>{setIsActive(!isActive); return archiveUnarchive(_id)}} className="archiveBorder">Activate</button>
						          }
						          </td>
						          <td className="tableRow2">
						          <a href={`/updateProduct/${_id}`} className="btn btn-primary btn-secondary shadow-none">Edit Product</a>
						          </td>
						        </tr>
						      </tbody>
						    </Table>
					</div>
				</Col>
			</Row>
		</Container>
		)
}
