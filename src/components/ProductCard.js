import {useState, useEffect, useContext} from 'react'

import {Container, Row, Col, Card, Button} from 'react-bootstrap'
import UserContext from '../UserContext'
import {Link} from 'react-router-dom'
import {Navigate} from 'react-router-dom';


export default function ProductCard(prop){ 

	const {_id, productName, description, price, stocks, action} = prop.prop;
	console.log(_id)

	const [stocksAvailable, setStocksAvailable] = useState(stocks)
	const [isAvailable, setIsAvailable] = useState(true);

	const {user} = useContext(UserContext)

	useEffect(()=>{
		if(stocksAvailable === 0){
			setIsAvailable(false);
			console.log(isAvailable);
		}
	}, [stocksAvailable]);


	function checkOut(){
		if(stocksAvailable === 1){
			alert("Congratulations, you just bought the last item!");
		}
		setStocksAvailable(stocksAvailable-1);
	}

	return(

		<Row className = "mt-3">
			<Col xs = {12} md = {4} className = "offset-md-4 offset-0">
				<Card>
				      <Card.Body>
				        <Card.Title>{productName}</Card.Title>

				        <Card.Subtitle>Description:</Card.Subtitle>
				        <Card.Text>
				         	{description}
				        </Card.Text>

				        <Card.Subtitle>Price:</Card.Subtitle>
				        <Card.Text>
				         	{price}
				        </Card.Text>

				        <Card.Subtitle>Slots Available:</Card.Subtitle>
				        <Card.Text>
				         	{stocksAvailable} stocks
				        </Card.Text>
				        	{
				        	(user !== null) ?
				        	<Button variant="dark"
					        	 as={Link} to={`/products/${_id}`}
					        	 variant="primary"
					        	 disabled = {!isAvailable} 
					        	 >View Details</Button>
				        	 :
						    <Button as={Link} to='/login' variant="dark" 
						       disabled={!isAvailable}
						       >Check out</Button>
				        	}
				      </Card.Body>
				    </Card>
			</Col>
		</Row>

		)
}
