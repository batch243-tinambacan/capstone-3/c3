import AppNavBar from './components/AppNavBar';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import PageError from './pages/PageError';
import Admin from './pages/Admin';
import CreateProduct from './pages/CreateProduct';
import AdminDashboard from './components/AdminDashboard';
import Products from './pages/Products';
import ProductView from './pages/ProductView';
import UpdateProduct from './pages/UpdateProduct';

import {UserContext} from './UserContext';
import {UserProvider} from './UserContext'

import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import {useParams, useNavigate} from 'react-router-dom';
import {useState, useEffect} from 'react';


import './App.css';

function App() {

  const [user, setUser] = useState({id: null, isAdmin: false})

  const unSetUser = ()=>{
    localStorage.clear()
  }

  useEffect(()=>{
    console.log(user)
  }, [user])

  useEffect(()=>{
    fetch(`${process.env.REACT_APP_URI}/users/profile`,
      {headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }})
    .then(response=> response.json())
    .then(data =>{
      console.log(data);
      setUser({id: data._id, isAdmin: data.isAdmin});
    })
  }, [])


  return (
    <UserProvider value={{user, setUser, unSetUser}}>
      <Router>
        <AppNavBar/>
        <Routes>
            <Route path="/" element={<Home/>}/>
            <Route path="/products" element={<Products/>}/>
            <Route path="/products/:productId/" element={<ProductView/>}/>
            <Route path="/updateProduct/:productId/" element={<UpdateProduct/>}/>
            <Route path="/login" element={<Login/>}/>
            <Route path="/register" element={<Register/>}/>
            <Route path="/logout" element={<Logout/>}/>
            <Route path="*" element={<PageError/>}/>
            <Route path="/admin" element={<Admin/>}/>
            <Route path="/createProduct" element={<CreateProduct/>}/>
            <Route path="/updateProduct" element={<UpdateProduct/>}/>
        </Routes>
      </Router>
    </UserProvider>
  );
}

export default App;
